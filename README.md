# BI-PST homework

## 1

59 jednotlivych udaju, 24 perished, 35 survived

### Skupina 1 perished

- EX: 727.9167?
- var: 106?
- med: 738?

### Skupina 2 survived

- EX: 738?
- var: 93?
- med: 739?

## 2

hist(case0201[case0201\$StatusId == 0,][,1], main="Skupina 1 - Perished", xlab="Humerus length")

F <- ecdf(case0201[case0201\$StatusId == 0,][,1])

plot(F, main="Skupina 1 - Perished")



png("s2.png")

dev.off()

### Skupina 1 perished

![histogram skupina 1](./hist_1.png)
![dist funkce skupina 1](./emp_dist_f_1.png)

### Skupina 2 survived

![histogram skupina 2](./hist_2.png)
![dist funkce skupina 2](./emp_dist_f_2.png)
